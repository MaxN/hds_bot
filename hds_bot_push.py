import os
import telegram
import json
import requests
import re
import time


TOKEN = os.environ['HDS_BOT_TOKEN']
REMOTE_URL = os.environ['HDS_REMOTE_URL']
CHAT_ID = os.environ['HDS_CHAT_ID']
config = {}


def fetch_data():
  r = requests.get(REMOTE_URL)
  r.raise_for_status()
  objs = re.findall('\[ORDER\].*?\[ID\](.*?)\[\/ID\].*?\[DATATIME\](.*?)\[\/DATATIME\].*?\[GO\](.*?)\[\/GO\].*?\[MEST\](.*?)\[\/MEST\].*?\[\/ORDER\]', r.text, re.IGNORECASE|re.MULTILINE|re.DOTALL)

  return objs

def send2telegram(id, datetime, place,seats):
  s = f"\U0001F550 Дата и время: {datetime}\n\U0001F697 Маршрут: {place}\n\U0001F4BA Пассажиров: {seats}"
  print(s)
  bot.send_message(chat_id=CHAT_ID, text=s)

if __name__ == '__main__':

  if os.path.exists("config.json"):
    config = json.loads(open("config.json").read())
  offset = config.get('offset')
  #print(f"REMOTE_URL {REMOTE_URL}")
  bot = telegram.Bot(token=TOKEN)
  print(bot.get_webhook_info())
  updates = bot.get_updates(offset=offset,allowed_updates=[])
  print(f"Got {len(updates)} updates")
  for u in updates:
    offset = u.update_id
    print(f"{u.effective_chat.id}:{u.message.text}")
    config['offset'] = offset
  config['offset'] += 1
  data = fetch_data()
  for r in data:
    send2telegram(id=r[0], datetime=r[1], place=r[2], seats=r[3])
    r = requests.post(REMOTE_URL, data = {'id': id, 'time': int(time.time())})
    r.raise_for_status()
    time.sleep(1)

  if len(data) > 0:
    bot.send_message(chat_id=CHAT_ID, text="www.HelpDoc.online")
  open("config.json", 'w').write(json.dumps(config))